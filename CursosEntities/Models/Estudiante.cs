using System;
using System.Collections.Generic;

namespace CursosEntities.Models
{
    public class Estudiante
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string ApellidoMaterno { get; set; }

        public string ApellidoPaterno { get; set; }

        public DateTime FechaDeNacimieto { get; set; }

        public DateTime FechaDeAlta { get; set; }

        //Foreing key a la tabla estatus
        public int EstatusId { get; set; }
        public Estatus Estatus { get; set; }

        public IEnumerable<CursosEstudiantes> CursosEstudiantes { get; set; }
        
    }
}
