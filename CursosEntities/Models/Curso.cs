using System;
using System.Collections.Generic;

namespace CursosEntities.Models
{
    public class Curso
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Objetivo { get; set; }    

        public DateTime FechaDeAlta { get; set; }

        //Foreing key a la tabla estatus
        public int EstatusId { get; set; }
        public Estatus Estatus { get; set; }

//Foreing key a la tabla Profesor
        public int ProfesorId { get; set; }
        public Profesor Profesor { get; set; }

        public IEnumerable<CursosEstudiantes> CursosEstudiantes { get; set; }
        
        
    }
}
