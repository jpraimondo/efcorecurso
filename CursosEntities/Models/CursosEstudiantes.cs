using System;
using System.Collections.Generic;

namespace CursosEntities.Models
{
    public class CursosEstudiantes
    {
        
        
        
        public int EstudianteId { get; set; }
        public Estudiante Estudiante { get; set; }
        
        
        public int CursoId { get; set; }
        public Curso Curso { get; set; }
        
        
        
        
    }

}