using System;
using System.Collections.Generic;

namespace CursosEntities.Models
{
    public class Profesor
    {
public int Id { get; set; }
public string Nombre { get; set; }

public string Apellidos { get; set; }
public DateTime FechaDeAlta { get; set; }

//Foreing key a la tabla estatus
        public int EstatusId { get; set; }
        public Estatus Estatus { get; set; }

//Foreing key a la tabla estatus
public IEnumerable<Curso> Cursos { get; set; }


    }
}
