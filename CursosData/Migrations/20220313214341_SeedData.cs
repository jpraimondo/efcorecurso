﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CursosData.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Estatus",
                columns: new[] { "Id", "Descripcion", "FechaDeAlta", "Nombre" },
                values: new object[] { 1, "Estatus del registro creado", new DateTime(2022, 3, 13, 18, 43, 40, 486, DateTimeKind.Local).AddTicks(6539), "Creado" });

            migrationBuilder.InsertData(
                table: "Estatus",
                columns: new[] { "Id", "Descripcion", "FechaDeAlta", "Nombre" },
                values: new object[] { 2, "Estatus del registro activo", new DateTime(2022, 3, 13, 18, 43, 40, 488, DateTimeKind.Local).AddTicks(3635), "Activo" });

            migrationBuilder.InsertData(
                table: "Estatus",
                columns: new[] { "Id", "Descripcion", "FechaDeAlta", "Nombre" },
                values: new object[] { 3, "Estatus del registro inactivo", new DateTime(2022, 3, 13, 18, 43, 40, 488, DateTimeKind.Local).AddTicks(3680), "Inactivo" });

            migrationBuilder.InsertData(
                table: "Estudiantes",
                columns: new[] { "Id", "ApellidoMaterno", "ApellidoPaterno", "EstatusId", "FechaDeAlta", "FechaDeNacimieto", "Nombre" },
                values: new object[] { 1, "Fernandez", "Gonzalez", 2, new DateTime(2022, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(7714), new DateTime(1995, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(6995), "Mariano" });

            migrationBuilder.InsertData(
                table: "Estudiantes",
                columns: new[] { "Id", "ApellidoMaterno", "ApellidoPaterno", "EstatusId", "FechaDeAlta", "FechaDeNacimieto", "Nombre" },
                values: new object[] { 2, "Fernandez", "Gonzalez", 2, new DateTime(2022, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(8737), new DateTime(1999, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(8714), "Romina" });

            migrationBuilder.InsertData(
                table: "Estudiantes",
                columns: new[] { "Id", "ApellidoMaterno", "ApellidoPaterno", "EstatusId", "FechaDeAlta", "FechaDeNacimieto", "Nombre" },
                values: new object[] { 3, "Fernandez", "Manrique", 2, new DateTime(2022, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(8744), new DateTime(1997, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(8740), "Pedro" });

            migrationBuilder.InsertData(
                table: "Estudiantes",
                columns: new[] { "Id", "ApellidoMaterno", "ApellidoPaterno", "EstatusId", "FechaDeAlta", "FechaDeNacimieto", "Nombre" },
                values: new object[] { 4, "Fernandez", "Bonelli", 2, new DateTime(2022, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(8751), new DateTime(1994, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(8747), "Marcelo" });

            migrationBuilder.InsertData(
                table: "Profesores",
                columns: new[] { "Id", "Apellidos", "EstatusId", "FechaDeAlta", "Nombre" },
                values: new object[] { 1, "Perez Gonzalez", 2, new DateTime(2022, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(2395), "Juan" });

            migrationBuilder.InsertData(
                table: "Profesores",
                columns: new[] { "Id", "Apellidos", "EstatusId", "FechaDeAlta", "Nombre" },
                values: new object[] { 2, "Perez Gonzalez", 2, new DateTime(2022, 3, 13, 18, 43, 40, 490, DateTimeKind.Local).AddTicks(3527), "Mariano" });

            migrationBuilder.InsertData(
                table: "Cursos",
                columns: new[] { "Id", "Descripcion", "EstatusId", "FechaDeAlta", "Nombre", "Objetivo", "ProfesorId" },
                values: new object[] { 1, "Curso de Matematica", 2, new DateTime(2022, 3, 13, 18, 43, 40, 491, DateTimeKind.Local).AddTicks(1228), "Matematica", null, 1 });

            migrationBuilder.InsertData(
                table: "Cursos",
                columns: new[] { "Id", "Descripcion", "EstatusId", "FechaDeAlta", "Nombre", "Objetivo", "ProfesorId" },
                values: new object[] { 2, "Curso de lengua", 2, new DateTime(2022, 3, 13, 18, 43, 40, 491, DateTimeKind.Local).AddTicks(2647), "Lengua", null, 1 });

            migrationBuilder.InsertData(
                table: "Cursos",
                columns: new[] { "Id", "Descripcion", "EstatusId", "FechaDeAlta", "Nombre", "Objetivo", "ProfesorId" },
                values: new object[] { 3, "Curso de fisica", 2, new DateTime(2022, 3, 13, 18, 43, 40, 491, DateTimeKind.Local).AddTicks(2657), "Fisica", null, 2 });

            migrationBuilder.InsertData(
                table: "CursosEstudiantes",
                columns: new[] { "CursoId", "EstudianteId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "CursosEstudiantes",
                columns: new[] { "CursoId", "EstudianteId" },
                values: new object[] { 1, 2 });

            migrationBuilder.InsertData(
                table: "CursosEstudiantes",
                columns: new[] { "CursoId", "EstudianteId" },
                values: new object[] { 2, 3 });

            migrationBuilder.InsertData(
                table: "CursosEstudiantes",
                columns: new[] { "CursoId", "EstudianteId" },
                values: new object[] { 3, 4 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CursosEstudiantes",
                keyColumns: new[] { "CursoId", "EstudianteId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "CursosEstudiantes",
                keyColumns: new[] { "CursoId", "EstudianteId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "CursosEstudiantes",
                keyColumns: new[] { "CursoId", "EstudianteId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "CursosEstudiantes",
                keyColumns: new[] { "CursoId", "EstudianteId" },
                keyValues: new object[] { 3, 4 });

            migrationBuilder.DeleteData(
                table: "Estatus",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Estatus",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Cursos",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Cursos",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Cursos",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Estudiantes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Estudiantes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Estudiantes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Estudiantes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Profesores",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Profesores",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Estatus",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
