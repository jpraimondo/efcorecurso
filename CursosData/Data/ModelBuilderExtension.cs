using Microsoft.EntityFrameworkCore;
using CursosEntities.Models;
using   System;
namespace CursosData.Data

{

    public static class ModelBuilderExtension
    {
        public static void Seed( this ModelBuilder modelBuilder)
        {
            //Estatus

            modelBuilder.Entity<Estatus>()
            .HasData(
                new Estatus {Id=1,Nombre="Creado",Descripcion="Estatus del registro creado",FechaDeAlta=System.DateTime.Now},
                new Estatus {Id=2,Nombre="Activo",Descripcion="Estatus del registro activo",FechaDeAlta=System.DateTime.Now},
                new Estatus {Id=3,Nombre="Inactivo",Descripcion="Estatus del registro inactivo",FechaDeAlta=System.DateTime.Now}
            );

            //Profesor
            modelBuilder.Entity<Profesor>()
            .HasData(
                new Profesor {Id=1,Nombre="Juan",Apellidos="Perez Gonzalez",FechaDeAlta=DateTime.Now,EstatusId=2 },
                new Profesor {Id=2,Nombre="Mariano",Apellidos="Perez Gonzalez",FechaDeAlta=DateTime.Now,EstatusId=2 }

            );

            //Estudiante
            modelBuilder.Entity<Estudiante>()
            .HasData(
                new Estudiante {Id=1,Nombre="Mariano",ApellidoPaterno="Gonzalez",ApellidoMaterno="Fernandez",FechaDeNacimieto=DateTime.Now.AddYears(-27),FechaDeAlta=DateTime.Now,EstatusId=2 },
                new Estudiante {Id=2,Nombre="Romina",ApellidoPaterno="Gonzalez",ApellidoMaterno="Fernandez",FechaDeNacimieto=DateTime.Now.AddYears(-23),FechaDeAlta=DateTime.Now,EstatusId=2 },
                new Estudiante {Id=3,Nombre="Pedro",ApellidoPaterno="Manrique",ApellidoMaterno="Fernandez",FechaDeNacimieto=DateTime.Now.AddYears(-25),FechaDeAlta=DateTime.Now,EstatusId=2 },
                new Estudiante {Id=4,Nombre="Marcelo",ApellidoPaterno="Bonelli",ApellidoMaterno="Fernandez",FechaDeNacimieto=DateTime.Now.AddYears(-28),FechaDeAlta=DateTime.Now,EstatusId=2 }
            );

            //Cursos
            modelBuilder.Entity<Curso>()
            .HasData(
                new Curso {Id=1,Nombre="Matematica",Descripcion="Curso de Matematica",FechaDeAlta=DateTime.Now,EstatusId=2,ProfesorId=1 },
                new Curso {Id=2,Nombre="Lengua",Descripcion="Curso de lengua",FechaDeAlta=DateTime.Now,EstatusId=2,ProfesorId=1 },
                new Curso {Id=3,Nombre="Fisica",Descripcion="Curso de fisica",FechaDeAlta=DateTime.Now,EstatusId=2,ProfesorId=2}

            );

            modelBuilder.Entity<CursosEstudiantes>()
            .HasData(
                new CursosEstudiantes {EstudianteId=1,CursoId=1},
                new CursosEstudiantes {EstudianteId=2,CursoId=1},
                new CursosEstudiantes {EstudianteId=3,CursoId=2},
                new CursosEstudiantes {EstudianteId=4,CursoId=3}

            );
        }
    }
}