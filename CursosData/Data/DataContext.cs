using Microsoft.EntityFrameworkCore;
using CursosEntities.Models;

namespace CursosData.Data
{

    public class DataContext:DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
        :base(options)
        {

        }

        public DbSet<Estatus> Estatus {get;set;}
        public DbSet<Profesor> Profesores { get; set; }

        public DbSet<Estudiante> Estudiantes { get; set; }
        
        public DbSet<Curso> Cursos { get; set; }
        
        public DbSet<CursosEstudiantes> CursosEstudiantes { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
{
    //Definir llave primaria

    builder.Entity<CursosEstudiantes>().HasKey( ce => new{ce.CursoId,ce.EstudianteId});

//Definir las relaciones de la tabla Cursos

    builder.Entity<CursosEstudiantes>()
    .HasOne<Curso>(ce=>ce.Curso)
    .WithMany(c=> c.CursosEstudiantes)
    .HasForeignKey(ce=> ce.CursoId);


    //Definir las relaciones de la tabla
    //Estudiantes
    
     builder.Entity<CursosEstudiantes>()
    .HasOne<Estudiante>(ce=>ce.Estudiante)
    .WithMany(e=> e.CursosEstudiantes)
    .HasForeignKey(ce=> ce.EstudianteId);

    //cargar datos iniciales
    builder.Seed();
}
        
        
        
    }

}
